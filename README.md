# Github Chrome

## Development version

To install the development version and hack on this yourself;

```bash
git clone https://bitbucket.org/shashank_shekhar_inox/github-chrome.git
cd github-chrome
npm install coffee-script -g
npm install haml-coffee mocha should
```
You need to install compass, there are two ways you can do that
```bash
npm install compass
```
or if you have ruby installed
```bash
gem install compass
```
The first method doesn't seem to install compass to the shell path (atleast not on OSX). The second one works fine.

Then run :
```bash
cake build
cake watch
```

Then, install the extension

- Open Chrome's extension tab by visiting chrome://extensions
- Check "Developer mode" is ticked
- Click "Load unpacked extension..."
- Select the root of the cloned repository
