###
two classes below are used in the onChange function
###

class @AssigneeCollection extends Backbone.Collection
  initialize: (@models, @options) ->

class @MilestoneCollection extends Backbone.Collection
  initialize: (@models, @options) ->


class @NewIssueView extends Backbone.View

  className: 'new-issue'

  events:
    "submit form" : "onSubmit"
    "change [name=repository]" : "onChange"

  initialize: (@options) ->
    @repositories = @options.repositories

  render: ->
    @$el.html(HAML['new_issue'](repositories: @repositories))
    @$('select').select2()



  ###
  The function below populates the assignee and milestone list 
  when a new repo is selected
  ###

  onChange: (e) ->
    name = @$("[name=repository]").val()
    localStorage['new_issue_last_repo'] = name
    assignee_url = "https://api.github.com/repos/"+ name + "/assignees"
    milestone_url = "https://api.github.com/repos/"+ name + "/milestones"
    token = localStorage['oauth2_github']['accessToken']
    
    ###
    For assignees 
    ###

    a_collection = new AssigneeCollection([], {url : assignee_url})
    assign_select = $("[name=assignee]")
    assign_select.find('option').remove().end().append("<option></option>")

    a_collection.fetch
      headers : @token
      success: =>
        a_collection.each (model)->
          $('<option>').text(model.get('login')).val(model.get('login')).appendTo(assign_select)
        @$('.assignee_message').html("<span>#{a_collection.length} Assignee(s) found!</span>")
      error: =>
        @$('.assignee_message').html("<span>Can't find Assignee(s)</span>")

    ###
    For milestones
    ###

    m_collection = new MilestoneCollection([], {url : milestone_url})
    milestone_select = $("[name=milestone]")
    milestone_select.find('option').remove().end().append("<option></option>")
    
    m_collection.fetch
      headers : @token
      success: =>
        m_collection.each (model)->
          $('<option>').text(model.get('title')).val(model.get('number')).appendTo(milestone_select)
        @$('.milestone_message').html("<span>#{m_collection.length} Milestone(s) found!</span>")       
      error: =>
        @$('.milestone_message').html("<span>Can't find  Milestone(s)</span>")



  onSubmit: (e) ->
    e.preventDefault()
    name = @$("[name=repository]").val()
    localStorage['new_issue_last_repo'] = name
    repository = @repositories.find (r) -> r.get('full_name') == name

    model = new IssueModel({
      body: @$("[name=body]").val() 
      title: @$("[name=title]").val()
      assignee: @$("[name=assignee]").val() if @$("[name=assignee]").val() != '' 
      milestone: @$("[name=milestone]").val() if @$("[name=milestone]").val() != '' 

    }, {repository: repository})


    model.save {},
      success: (model) =>
        @badge = new Badge()
        @badge.addIssues(1)
        @$('.message').html("<span>Issue <a href=\"#{model.get("html_url")}\" target=\"_blank\">##{model.get('number')}</a> was created!</span>")
      error: =>
        @$('.message').html("<span>Failed to create issue :(</span>")

